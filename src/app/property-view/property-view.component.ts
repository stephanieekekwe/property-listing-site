import { Component, OnInit, ViewChild } from '@angular/core';
import { Property } from '../property.model';
import { AppDataService } from '../app-data.service';
import { isNullOrUndefined } from 'util';
import { ActivatedRoute } from '@angular/router';
import { NgbCarousel, NgbSlideEvent, NgbSlideEventSource } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-property-view',
  templateUrl: './property-view.component.html',
  styleUrls: ['./property-view.component.css']
})
export class PropertyViewComponent implements OnInit {
  propdata: Property[] = this.data.getData().propertyDetails;
  property_details: Property;
  id: number;
  galleryImage: any[];
  images: any[];
  paused = false;
  unpauseOnArrow = false;
  pauseOnIndicator = false;
  pauseOnHover = true;
  @ViewChild('carousel', { static: true }) carousel: NgbCarousel;

  togglePaused() {
    if (this.paused) {
      this.carousel.cycle();
    } else {
      this.carousel.pause();
    }
    this.paused = !this.paused;
  }

  onSlide(slideEvent: NgbSlideEvent) {
    if (this.unpauseOnArrow && slideEvent.paused &&
      (slideEvent.source === NgbSlideEventSource.ARROW_LEFT || slideEvent.source === NgbSlideEventSource.ARROW_RIGHT)) {
      this.togglePaused();
    }
    if (this.pauseOnIndicator && !slideEvent.paused && slideEvent.source === NgbSlideEventSource.INDICATOR) {
      this.togglePaused();
    }
  }
  constructor(public data: AppDataService, private route: ActivatedRoute) { }

  ngOnInit() {
    this.id = this.route.snapshot.params['id'];
    const selectedPropertyId = this.id - 1;
    this.property_details = this.propdata[selectedPropertyId];
    this.galleryImage = this.propdata[selectedPropertyId].imageGallery;
    console.log(this.galleryImage.toString());
    console.log(this.property_details);

    this.images = this.galleryImage;

  }

  getDetails() {
  }
}
