import { Component, OnInit } from '@angular/core';
import { Property } from '../property.model';
import { AppDataService } from '../app-data.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-landing',
  templateUrl: './landing.component.html',
  styleUrls: ['./landing.component.css']
})
export class LandingComponent implements OnInit {
  landingText = 'Mordern Housing ';
  propdata: Property[] = this.data.getData().propertyDetails;

  constructor(public data: AppDataService,
   private router: Router) { }

  ngOnInit() {
    console.log(this.data);
  }

  onView(property) {
    property.id;
    console.log(property.id);
    this.router.navigate(['/view-details/' + property.id]);
  }
}
