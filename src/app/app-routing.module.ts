import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LandingComponent } from './landing/landing.component';
import { PropertyViewComponent } from './property-view/property-view.component';

const routes: Routes = [

  { path: '', component: LandingComponent },
  { path: 'view-details/:id', component: PropertyViewComponent },

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
