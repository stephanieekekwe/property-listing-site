import { Gallery } from './Gallery.model';
import { ContactDetails } from './ContactDetails.model';
import { BuildingDetails } from './building.model';

export class Property {
  public Id: number;
  public name: string;
  public imagePath: string;
  public location: string;
  public price: number;
  public contactDetails: ContactDetails;
  public imageGallery: Gallery[];
  public buildingDetails: BuildingDetails;
}
