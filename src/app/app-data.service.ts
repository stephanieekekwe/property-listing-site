import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import  data from '../assets/data.json';
@Injectable({
  providedIn: 'root'
})
export class AppDataService {

  private appData;
  dataUrl = '../assets/data.json';
  constructor(private http: HttpClient) { }

  loadAppData() {
    return this.appData = data;
  }

  getData() {
    console.log('calling the data ');
    this.appData = this.loadAppData();
    // this.appData = config;
    console.log('got the data ', this.appData);
    return this.appData;
  }
}
