import { BrowserModule } from '@angular/platform-browser';
import { NgModule, APP_INITIALIZER } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { NgbCarouselModule, NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavbarComponent } from './navbar/navbar.component';
import { LandingComponent } from './landing/landing.component';
import { AppDataService } from './app-data.service';
import { PropertyViewComponent } from './property-view/property-view.component';
import { FormsModule } from '@angular/forms';
import { FooterComponent } from './footer/footer.component';

const appInitializerFn = (appData: AppDataService) => {
  return () => {
    return appData.loadAppData();
  };
};

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    LandingComponent,
    PropertyViewComponent,
    FooterComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    NgbModule,
    FormsModule,
    FontAwesomeModule
  ],
  providers: [
    AppDataService,
    {
      provide: APP_INITIALIZER,
      useFactory: appInitializerFn,
      multi: true,
      deps: [AppDataService]
    },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
